import { combineReducers } from 'redux';
import { socialAuth } from '../components/organisms/SocialAuth/reducer';

const rootReducer = combineReducers({
  socialAuth,
});

export default history => rootReducer;
