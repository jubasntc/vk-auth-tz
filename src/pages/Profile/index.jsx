import * as React from 'react';
import { Page } from '../../components/templates/Page';
import ProfileView from '../../components/organisms/ProfileView/container';

const Profile = () => {
  return (
    <Page>
      <ProfileView />
    </Page>
  );
};

export { Profile };
