import { connect } from 'react-redux';
import { Landing } from './index';

const mapStateToProps = state => ({
  token: state.socialAuth.token,
});

export default connect(mapStateToProps)(Landing);
