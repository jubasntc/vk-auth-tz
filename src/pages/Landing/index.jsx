import * as React from 'react';
import { Redirect } from 'react-router-dom';
import SocialAuth from '../../components/organisms/SocialAuth/container';
import { Page } from '../../components/templates/Page';

const Landing = ({ token }) => {
  return (
    <React.Fragment>
      {token ? (
        <Redirect to="/profile" />
      ) : (
        <Page>
          <SocialAuth />
        </Page>
      )}
    </React.Fragment>
  );
};

export { Landing };
