import * as React from 'react';
import { Page } from '../../components/templates/Page';
import { Header, Icon } from 'semantic-ui-react';

const NotFound = () => {
  return (
    <Page>
      <Header as="h1" icon>
        <Icon name="settings" />
        Error 404
        <Header.Subheader>Page not found</Header.Subheader>
      </Header>
    </Page>
  );
};

export { NotFound };
