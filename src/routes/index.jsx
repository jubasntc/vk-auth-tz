import * as React from 'react';
import PrivateRoute from './PrivateRoute';
import Landing from '../pages/Landing/container';
import { Route, Switch } from 'react-router-dom';
import { Profile } from '../pages/Profile';
import { NotFound } from '../pages/NotFound';

const Routes = () => (
  <Switch>
    <Route exact path="/" component={Landing} />
    <PrivateRoute path="/profile" component={Profile} />
    <Route component={NotFound} />
  </Switch>
);

export { Routes };
