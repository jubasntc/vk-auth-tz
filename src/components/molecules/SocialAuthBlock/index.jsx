import * as React from 'react';
import PropTypes from 'prop-types';
import { VkAuth } from '../../../lib/VkAuth';

const SocialAuthBlock = ({ singInVk }) => (
  <>
    <VkAuth apiId={process.env.REACT_APP_VK_API_ID} callback={singInVk} />
    <h4>Тестовое задание от Emphasoft</h4>
  </>
);

SocialAuthBlock.propTypes = {
  singInVk: PropTypes.func.isRequired,
};

export { SocialAuthBlock };
