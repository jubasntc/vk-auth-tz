import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'semantic-ui-react';
import { FriendRow } from '../../atoms/FriendRow';

const FriendsList = ({ friendsList }) => (
  <List selection verticalAlign="middle">
    {friendsList.map(friend => (
      <FriendRow
        key={friend.id}
        first_name={friend.first_name}
        last_name={friend.last_name}
        photo_100={friend.photo_100}
      />
    ))}
  </List>
);

FriendsList.propTypes = {
  friendsList: PropTypes.arrayOf({
    first_name: PropTypes.string.isRequired,
    last_name: PropTypes.string.isRequired,
    photo_100: PropTypes.string.isRequired,
  }).isRequired,
};

export { FriendsList };
