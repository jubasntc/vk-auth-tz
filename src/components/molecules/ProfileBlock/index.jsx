import React from 'react';
import PropTypes from 'prop-types';
import { Button, Segment } from 'semantic-ui-react';
import { FriendsList } from '../FriendsList';

const ProfileBlock = ({ doLogout, displayName, friendsList }) => (
  <Segment>
    <h2>{displayName}</h2>
    <h4>Cписок друзей:</h4>
    <FriendsList friendsList={friendsList} />
    <Button primary size="large" onClick={doLogout}>
      Выйти
    </Button>
  </Segment>
);

ProfileBlock.propTypes = {
  doLogout: PropTypes.func.isRequired,
  displayName: PropTypes.string.isRequired,
  friendsList: PropTypes.array.isRequired,
};

export { ProfileBlock };
