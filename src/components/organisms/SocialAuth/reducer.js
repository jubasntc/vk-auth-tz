import { SUCCESS_AUTH, FAILURE_AUTH, LOGOUT } from './actions';

const initialState = {
  token: null,
  uid: null,
  displayName: '',
  friendsList: [],
  error: '',
};

const socialAuth = (state = initialState, action) => {
  switch (action.type) {
    case SUCCESS_AUTH:
      return {
        ...state,
        ...action.userInfo,
        error: '',
      };

    case FAILURE_AUTH:
      return {
        ...state,
        error: action.payload.message,
      };

    case LOGOUT:
      return initialState;

    default:
      return state;
  }
};

export { socialAuth };
