import * as React from 'react';
import PropTypes from 'prop-types';
import { SocialAuthBlock } from '../../molecules/SocialAuthBlock';

const SocialAuth = ({ singInVk }) => {
  return <SocialAuthBlock singInVk={singInVk} />;
};

SocialAuth.propTypes = {
  singInVk: PropTypes.func.isRequired,
};

export { SocialAuth };
