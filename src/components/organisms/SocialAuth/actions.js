export const SUCCESS_AUTH = 'SUCCESS_AUTH';
export const FAILURE_AUTH = 'FAILURE_AUTH';
export const LOGOUT = 'LOGOUT';
export const VK_FRIEDS_GET = 'friends.get';
export const VK_ORDER = 'random';
export const START_POSITION = 0;
export const COUNT_FRIENDS = 5;
export const VK_PHOTO_100 = 'photo_100';
export const VK_VERSION = '5.102';

export const singInVk = response => {
  return dispatch => {
    window.VK.api(
      VK_FRIEDS_GET,
      {
        user_ids: response.session.user.id,
        order: VK_ORDER,
        v: VK_VERSION,
        fields: VK_PHOTO_100,
      },
      data => {
        const activeFriends = data.response.items
          .filter(friend => !!!friend.deactivated)
          .slice(START_POSITION, COUNT_FRIENDS);
        dispatch(
          setSuccessAuth({
            token: response.session.sig,
            displayName: `${response.session.user.last_name} ${response.session.user.first_name}`,
            uid: `id${response.session.user.id}`,
            friendsList: activeFriends,
          })
        );
      }
    );
  };
};

export const doLogout = () => {
  return {
    type: LOGOUT,
  };
};

const setSuccessAuth = userInfo => {
  return {
    type: SUCCESS_AUTH,
    userInfo,
  };
};

export const setFailureAuth = e => {
  return {
    type: FAILURE_AUTH,
    payload: new Error(e),
  };
};
