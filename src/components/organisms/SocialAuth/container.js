import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { SocialAuth } from './index';
import { singInVk } from './actions';

const mapStateToProps = state => ({
  socialAuth: state.socialAuth,
});

const mapDispatchToProps = dispatch => ({
  singInVk: bindActionCreators(singInVk, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(SocialAuth);
