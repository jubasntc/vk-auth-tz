import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { ProfileView } from './index';
import { doLogout } from '../SocialAuth/actions';

const mapStateToProps = state => ({
  displayName: state.socialAuth.displayName,
  friendsList: state.socialAuth.friendsList,
});

const mapDispatchToProps = dispatch => ({
  doLogout: bindActionCreators(doLogout, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProfileView);
