import * as React from 'react';
import PropTypes from 'prop-types';
import { ProfileBlock } from '../../molecules/ProfileBlock';

const ProfileView = props => {
  return <ProfileBlock {...props} />;
};

ProfileView.propTypes = {
  doLogout: PropTypes.func.isRequired,
  displayName: PropTypes.string.isRequired,
  friendsList: PropTypes.array.isRequired,
};

export { ProfileView };
