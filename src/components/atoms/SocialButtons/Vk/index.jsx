import React from 'react';
import PropTypes from 'prop-types';
import { Button, Icon } from 'semantic-ui-react';

const VkButton = ({ callback }) => (
  <Button color="vk" size="large" onClick={callback}>
    <Icon name="vk" /> Авторизоваться
  </Button>
);

VkButton.propTypes = {
  callback: PropTypes.func.isRequired,
};

export { VkButton };
