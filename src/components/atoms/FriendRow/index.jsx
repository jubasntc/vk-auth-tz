import React from 'react';
import PropTypes from 'prop-types';
import { Image, List } from 'semantic-ui-react';

const FriendRow = ({ first_name, last_name, photo_100 }) => (
  <List.Item>
    <Image avatar src={photo_100} />
    <List.Content>
      <List.Header>
        {last_name} {first_name}
      </List.Header>
    </List.Content>
  </List.Item>
);

FriendRow.propTypes = {
  first_name: PropTypes.string.isRequired,
  last_name: PropTypes.string.isRequired,
  photo_100: PropTypes.string.isRequired,
};

export { FriendRow };
